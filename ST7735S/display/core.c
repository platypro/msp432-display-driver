/*
 * command.c
 *
 * ST7735S Core driver implementation for the MSP432.
 */

#include "display.h"

#pragma CHECK_MISRA("none")
#include <msp.h>
#include <stdbool.h>
#pragma RESET_MISRA("all")

typedef enum
{
    DISPLAY_OP_TYPE_NONE,
    DISPLAY_OP_TYPE_SINGLE_8,
    DISPLAY_OP_TYPE_SINGLE_16,
    DISPLAY_OP_TYPE_MULTIPLE,
    DISPLAY_OP_TYPE_COMMAND

} DISPLAY_OP_TYPE;

typedef struct
{
    /* I put len on the top because the compiler mistakes
     * 0-initializations to setting the enum
     */
    uint32_t len;
    DISPLAY_OP_TYPE type;
    /* This union is being used as a variant */
    #pragma CHECK_MISRA("-18.4")
    union {
        uint16_t single_16;
        uint8_t  single_8;
        uint8_t  command;
        uint8_t* multiple;
    };
    #pragma CHECK_MISRA("-18.4")
} DISPLAY_OP;

typedef struct dma_control_t_
{
    volatile void*        src_endptr;
    volatile void*        dst_endptr;
    volatile uint32_t control;
    volatile uint32_t reserved;

} dma_control_t;

/* These are accessed in other files, but the compiler
 * still complains
 */
#pragma CHECK_MISRA("-8.7")
display_status_t display_status;
#pragma RESET_MISRA("all")

static bool display_xfer_next(void);
static void display_setup_control_(dma_control_t* cfg, uint32_t config_control);
static void display_setup_control(uint32_t config_control);
static void display_pushByte(uint8_t byte);
static bool display_xfer_kick(void);
static bool display_op(DISPLAY_OP op);

void DMA_INT1_IRQHandler(void);

/* LCD pins */
#define PIN_LCD_SCK           ((uint32_t)BIT5)  /* P1.5 for UCB0 SCK connects to LCD SCK */
#define PIN_LCD_MOSI          ((uint32_t)BIT6)  /* P1.6 for UCB0 MOSI connects to LCD SDA */
#define PIN_LCD_RST           ((uint32_t)BIT7)  /* P5.7 connects to LCD RST pin */
#define PIN_LCD_CS            ((uint32_t)BIT0)  /* P5.0 connects to LCD CS pin */
#define PIN_LCD_DC            ((uint32_t)BIT7)  /* P3.7 connects to LCD DC pin */

#define PORT_LCD_SCK  (P1)
#define PORT_LCD_MOSI (P1)
#define PORT_LCD_RST  (P5)
#define PORT_LCD_CS   (P5)
#define PORT_LCD_DC   (P3)

/* DMA Structures */
#pragma DATA_ALIGN ( dma_control , 256 )
dma_control_t dma_control[16]; /* Big enough to hold both data control structures */

#define DMA_CONTROL_PRIMARY 0
#define DMA_CONTROL_SECONDARY 8

static DISPLAY_OP display_op_current = {0};
static DISPLAY_OP display_op_next = {0};
volatile bool display_is_busy = false;

void display_core_init(void)
{
    *((uint32_t*)&display_status) = 0x00610000U;
/* Register structs incoming */
#pragma CHECK_MISRA("-10.1")
    /* enable pins for UCB0 SPI MOSI/SCK */
    PORT_LCD_SCK->SEL0  |=  PIN_LCD_SCK;
    PORT_LCD_SCK->SEL1  &= ~PIN_LCD_SCK;

    PORT_LCD_MOSI->SEL0 |=  PIN_LCD_MOSI;
    PORT_LCD_MOSI->SEL1 &= ~PIN_LCD_MOSI;

    /* enable CS, RST and DC outputs */
    PORT_LCD_CS->DIR  |= PIN_LCD_CS;
    PORT_LCD_RST->DIR |= PIN_LCD_RST;
    PORT_LCD_DC->DIR  |= PIN_LCD_DC;

    /* activate CS */
    PORT_LCD_CS->OUT &= ~PIN_LCD_CS;

    /* ensure UCB0 SPI module in software reset */
    EUSCI_B0_SPI->CTLW0 |= EUSCI_B_CTLW0_SWRST;

    /* configure SPI as master, MSB first, SMCLK as clock source,
     *  data captured on first clock edge, low clock inactive state
     */
    EUSCI_B0_SPI->CTLW0 = EUSCI_B_CTLW0_MST
                        | EUSCI_B_CTLW0_MSB
                        | EUSCI_B_CTLW0_UCSSEL_2
                        | EUSCI_B_CTLW0_CKPH
                        | EUSCI_B_CTLW0_SWRST;

    /* take UCB0 SPI module out of software reset */
    EUSCI_B0_SPI->CTLW0 &= ~(uint32_t)EUSCI_B_CTLW0_SWRST;

    /* Reset the display with hardware reset */
    PORT_LCD_RST->OUT &= ~PIN_LCD_RST;
    PORT_LCD_RST->OUT |= PIN_LCD_RST;

    /* Enable DMA */
    DMA_Control->CFG = DMA_CFG_MASTEN;

    /* We are using The EUSCI B0 SPI TX bus.
     * Lets make sure the channel (#2) is disabled, then
     * configure it to use the SPI.
     * See P401R datasheet, pg. 115
     */
    DMA_Control->ENACLR = DISPLAY_DMA_CHANNEL_BIT;
    DMA_Channel->CH_SRCCFG[DISPLAY_DMA_CHANNEL] = DMA_EUSCI_B0_TX0;

    /* This is a data structure
     * containing information about source, destination, etc.
     * We need to point the DMA CTLBASE to this structure
     */
    DMA_Control->CTLBASE = (uint32_t)dma_control;

    /* I don't know what these features are, but I don't
     * need them right now.
     */
    DMA_Control->USEBURSTCLR = DISPLAY_DMA_CHANNEL_BIT;
    DMA_Control->REQMASKCLR  = DISPLAY_DMA_CHANNEL_BIT;

    /* Use the primary data structure. The alternate data
     * structure is for getting another DMA transaction
     * ready while the primary one is being processed
     */
    DMA_Control->ALTCLR      = DISPLAY_DMA_CHANNEL_BIT;

    /* Set channel to default priority */
    DMA_Control->PRIOCLR     = DISPLAY_DMA_CHANNEL_BIT;
#pragma RESET_MISRA("all")

    __NVIC_EnableIRQ(DMA_INT1_IRQn);

    display_op_current.type = DISPLAY_OP_TYPE_NONE;
    display_op_next.type = DISPLAY_OP_TYPE_NONE;
}

static bool display_xfer_next(void)
{
    /* This variable contains the index of the current control structure (primary or secondary) */
    static int32_t dma_control_at = DMA_CONTROL_PRIMARY;
    bool result = _true;

    dma_control[dma_control_at].control &= ~(UDMA_CHCTL_XFERMODE_PINGPONG);

    if(display_op_current.len == 0U)
        { result = _false; }
    else
    {
        uint32_t nextBytes;
        if(display_op_current.len >= 1024U)
        {
            nextBytes = 1023U;
            display_op_current.len -= 1024U;
        }
        else
        {
            nextBytes = display_op_current.len;
            display_op_current.len = 0U;
        }

        display_op_current.multiple += nextBytes;

        /* Set source pointer if the source increment is not none. */
        if((dma_control[dma_control_at].control & UDMA_CHCTL_SRCINC_NONE) != UDMA_CHCTL_SRCINC_NONE)
        {
            dma_control[dma_control_at].src_endptr = display_op_current.multiple;
        }

        display_op_current.multiple += 1U;

        /* Reset transfer size */
        dma_control[dma_control_at].control &= ~((uint32_t)1023U << UDMA_CHCTL_XFERSIZE_S);
        dma_control[dma_control_at].control |= (nextBytes << UDMA_CHCTL_XFERSIZE_S);

        dma_control[dma_control_at].control |= UDMA_CHCTL_XFERMODE_PINGPONG;

        /* swap control structure for next time */
        dma_control_at = (dma_control_at == DMA_CONTROL_PRIMARY) ? DMA_CONTROL_SECONDARY : DMA_CONTROL_PRIMARY;
    }

    return result;
}

static void display_setup_control_(dma_control_t* cfg, uint32_t config_control)
{
/* EUSCI_B0_BASE is not valid */
#pragma CHECK_MISRA("-10.1")
    /* Set destination address */
    cfg->dst_endptr = (void*) (EUSCI_B0_BASE + OFS_UCB0TXBUF);
#pragma RESET_MISRA("all")

    /* Set control word */
    cfg->control = config_control;
}

static void display_setup_control(uint32_t config_control)
{
    display_setup_control_(&dma_control[DMA_CONTROL_PRIMARY], config_control);
    display_setup_control_(&dma_control[DMA_CONTROL_SECONDARY], config_control);
}

static void display_pushByte(uint8_t byte)
{
/* Register structs incoming */
#pragma CHECK_MISRA("-10.1")
    /* Transmit byte */
    EUSCI_B0_SPI->TXBUF = byte;

    /* Wait to finish */
    while(EUSCI_B0_SPI->STATW & EUSCI_B_STATW_SPI_BUSY);
#pragma RESET_MISRA("all")

}

static bool display_xfer_kick(void)
{
/* Register structs incoming */
#pragma CHECK_MISRA("-10.1")
    bool result = _false;
    display_op_current = display_op_next;
    display_op_next.type = DISPLAY_OP_TYPE_NONE;

    if(display_op_current.type != DISPLAY_OP_TYPE_NONE)
    {
        /* Disable the DMA Channels */
        DMA_Control->ENACLR = DISPLAY_DMA_CHANNEL_BIT;

        switch(display_op_current.type)
        {
        case DISPLAY_OP_TYPE_COMMAND:
            /* Change to command mode */
            PORT_LCD_DC->OUT &= ~PIN_LCD_DC;

            display_pushByte(display_op_current.command);

            /* Revert to data mode */
            PORT_LCD_DC->OUT |=  PIN_LCD_DC;
            break;
        case DISPLAY_OP_TYPE_SINGLE_8:
        {
            display_pushByte(display_op_current.single_8);
            break;
        }
        case DISPLAY_OP_TYPE_SINGLE_16:
        {
            display_pushByte((display_op_current.single_16 & 0xFF00U) >> 8U);
            display_pushByte((display_op_current.single_16 & 0x00FFU));
            break;
        }
        case DISPLAY_OP_TYPE_MULTIPLE:
        {
            display_is_busy = true;
/* These constants are missing 'U's */
#pragma CHECK_MISRA("-10.6")
            uint32_t config_control =
                          UDMA_CHCTL_DSTINC_NONE       /* No destination increment */
                        | UDMA_CHCTL_DSTSIZE_8         /* Destination is 8 bits wide */
                        | UDMA_CHCTL_SRCINC_8          /* Next source symbol is after one byte */
                        | UDMA_CHCTL_SRCSIZE_8         /* Source is 8 bits wide */
                        | UDMA_CHCTL_ARBSIZE_1024      /* Arbitrate after all transfers */
                        | UDMA_CHCTL_XFERMODE_PINGPONG;/* Ping Pong transfer mode */
#pragma RESET_MISRA("10.6")

            display_setup_control(config_control);

            /* Push the first bytes to the dma config */
            display_xfer_next();
            display_xfer_next();

            DMA_Control->ENASET = DISPLAY_DMA_CHANNEL_BIT;
            DMA_Channel->INT1_SRCCFG = DISPLAY_DMA_CHANNEL | DMA_INT1_SRCCFG_EN;

            /* Trigger channel */
            DMA_Channel->SW_CHTRIG = DISPLAY_DMA_CHANNEL_BIT;

            result = _true;
            break;
        }
        default: break;
        }
    }
    return result;
#pragma RESET_MISRA("all")
}

void DMA_INT1_IRQHandler(void)
{
/* Register structs incoming */
#pragma CHECK_MISRA("-10.1")
    if(display_xfer_next())
    {
        /* Trigger channel 1 */
        DMA_Channel->SW_CHTRIG = DISPLAY_DMA_CHANNEL_BIT;
    }
    else
    {
        if(!display_xfer_kick())
        {
            /* Disable this interrupt */
            DMA_Channel->INT1_SRCCFG = 0;

            /* Disable the DMA if out of data */
            DMA_Control->ENACLR = BIT2;
        }
        display_is_busy = _false;
    }
#pragma RESET_MISRA("all")
}

static bool display_op(DISPLAY_OP op)
{
    /* Wait for Next op to be cleared. */
    while(display_op_next.type != DISPLAY_OP_TYPE_NONE);
    while(display_is_busy);

    display_op_next = op;

    display_xfer_kick();

    return _true;
}

void display_flush(void)
{
    while(display_op_next.type != DISPLAY_OP_TYPE_NONE);
    while(display_is_busy);
    while(display_op_current.type != DISPLAY_OP_TYPE_NONE);
}

bool display_write_single_8(uint8_t src)
{
    DISPLAY_OP op;
    op.type=DISPLAY_OP_TYPE_SINGLE_8;
    op.single_8 = src;
    op.len=1U;
    return display_op(op);
}

bool display_write_single_16(uint16_t src)
{
    DISPLAY_OP op;
    op.type=DISPLAY_OP_TYPE_SINGLE_16;
    op.single_16 = src;
    op.len=1U;
    return display_op(op);
}

bool display_write_multiple(uint8_t* src, uint32_t len)
{
    DISPLAY_OP op;
    op.type=DISPLAY_OP_TYPE_MULTIPLE;
    op.multiple = src;
    op.len=len;
    return display_op(op);
}

bool display_write_command(uint8_t command)
{
    DISPLAY_OP op;
    op.type=DISPLAY_OP_TYPE_COMMAND;
    op.command = command;
    op.len=1U;
    return display_op(op);
}

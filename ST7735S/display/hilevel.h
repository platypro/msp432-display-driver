/*
 * "display/hilevel.h"
 *
 * High Level Display Driver
 */

#ifndef DRIVER_HILEVEL_H_
#define DRIVER_HILEVEL_H_

#pragma CHECK_MISRA("none")
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include <string.h>
#pragma RESET_MISRA("all")

uint8_t display_min(uint8_t a, uint8_t b);
uint8_t display_max(uint8_t a, uint8_t b);

typedef struct
{
    unsigned int r  : 5;
    unsigned int g  : 6;
    unsigned int b  : 5;

} display_colour_t;

#define DISPLAY_ANCHOR_FIRST  (1U)
#define DISPLAY_ANCHOR_MIDDLE (2U)
#define DISPLAY_ANCHOR_LAST   (3U)
#define DISPLAY_ANCHOR_MASK   (3U)
#define DISPLAY_ANCHOR_HORI_SHIFT    (2U)

#define DISPLAY_ANCHOR_TOP_LEFT       (DISPLAY_ANCHOR_FIRST | (DISPLAY_ANCHOR_FIRST << DISPLAY_ANCHOR_HORI_SHIFT))
#define DISPLAY_ANCHOR_TOP_CENTER     (DISPLAY_ANCHOR_FIRST | (DISPLAY_ANCHOR_MIDDLE << DISPLAY_ANCHOR_HORI_SHIFT))
#define DISPLAY_ANCHOR_TOP_RIGHT      (DISPLAY_ANCHOR_FIRST | (DISPLAY_ANCHOR_LAST << DISPLAY_ANCHOR_HORI_SHIFT))
#define DISPLAY_ANCHOR_MIDDLE_LEFT    (DISPLAY_ANCHOR_MIDDLE | (DISPLAY_ANCHOR_FIRST << DISPLAY_ANCHOR_HORI_SHIFT))
#define DISPLAY_ANCHOR_MIDDLE_CENTER  (DISPLAY_ANCHOR_MIDDLE | (DISPLAY_ANCHOR_MIDDLE << DISPLAY_ANCHOR_HORI_SHIFT))
#define DISPLAY_ANCHOR_MIDDLE_RIGHT   (DISPLAY_ANCHOR_MIDDLE | (DISPLAY_ANCHOR_LAST << DISPLAY_ANCHOR_HORI_SHIFT))
#define DISPLAY_ANCHOR_BOTTOM_LEFT    (DISPLAY_ANCHOR_LAST | (DISPLAY_ANCHOR_FIRST << DISPLAY_ANCHOR_HORI_SHIFT))
#define DISPLAY_ANCHOR_BOTTOM_CENTER  (DISPLAY_ANCHOR_LAST | (DISPLAY_ANCHOR_MIDDLE << DISPLAY_ANCHOR_HORI_SHIFT))
#define DISPLAY_ANCHOR_BOTTOM_RIGHT   (DISPLAY_ANCHOR_LAST | (DISPLAY_ANCHOR_LAST << DISPLAY_ANCHOR_HORI_SHIFT))

typedef uint8_t display_anchor_t;

#define DISPLAY_COLOUR_BLACK ((display_colour_t){.r=0,  .g=0,  .b=0})
#define DISPLAY_COLOUR_WHITE ((display_colour_t){.r=31, .g=63, .b=31})
#define DISPLAY_COLOUR_RED   ((display_colour_t){.r=31, .g=0,  .b=0})
#define DISPLAY_COLOUR_GREEN ((display_colour_t){.r=0,  .g=63, .b=0})
#define DISPLAY_COLOUR_BLUE  ((display_colour_t){.r=0,  .g=0,  .b=31})

typedef struct display_render_region_t_
{
    uint8_t x1;
    uint8_t y1;

    uint8_t x2;
    uint8_t y2;

} display_render_region_t;

#define DISPLAY_RENDER_REGION_FULL ((display_render_region_t){.x1=0, .y1=0, .x2=127, .y2=127})

typedef uint8_t display_render_buffer_t;

#define EPSILON 0.0005

#define DISPLAY_BUFFER_LENGTH 256U
#define DISPLAY_BUFFER_SIZE (DISPLAY_BUFFER_LENGTH * 2U)
#define DISPLAY_NUM_BUFFERS 3U

#define DISPLAY_OFFSET_X (2U)
#define DISPLAY_OFFSET_Y (3U)

typedef void (*display_shader_t)(void* itri, display_render_region_t region, uint8_t* buf, uint32_t i);

typedef struct
{
    display_shader_t shader;
    void* next;

} display_shader_null_t;

/** @brief Get screen working on the MSP432 Launchpad + Educational Boosterpack V2
 */
extern bool display_init(void);

/** @brief Fills a shader buffer line with a colour. To be called within shaders.
 *  @param buf     The shader line buffer to fill
 *  @param startx  The first x position
 *  @param endx    The ending x position
 *  @param colour  The colour to fill with
 */
extern bool display_shader_set_line(uint8_t* buf, uint32_t startx, uint32_t endx, display_colour_t colour);

/** @brief Sets a shader buffer pixel to a colour. To be called within shaders.
 *  @param buf     The shader line buffer to fill
 *  @param x       X coordinate to set
 *  @param colour  The colour to fill with
 */
extern void display_shader_set_pixel(uint8_t buf[], uint32_t x, display_colour_t colour);

/** @brief Gets a shader colour from a shader buffer. To be called within shaders.
 *  @param buf     The shader line buffer get the colour from
 *  @param x       X coordinate to get
 *  @returns       The colour of the pixel
 */
extern display_colour_t display_shader_get_pixel(uint8_t buf[], uint32_t x);


/** @brief Set the "next" property of a shader
 *  @param render  The shader to set the field of
 *  @param next    The shader to set the field to
 */
extern void display_shader_set_next_(display_shader_null_t* render, display_shader_null_t* next);
#define display_shader_set_next(render, next) (display_shader_set_next_((display_shader_null_t*)(render), (display_shader_null_t*)(next)))

/** @brief Set the "shader" property of a shader
 *  @param render  The shader to set the field of
 *  @param shader  The shader to set the field to
 */
extern void display_shader_set_shader_(display_shader_null_t* render, display_shader_t shader);
#define display_shader_set_shader(render, shader) (display_shader_set_shader_((display_shader_null_t*)(render), (shader)))

/** @brief Clears the screen to a colour
 *  @param colour  The colour to clear to.
 */
extern void display_clear(display_colour_t colour);

/** @brief Sets a single pixel.
 *  @param x       The x coordinate of the pixel to set
 *  @param y       The y coordinate of the pixel to set
 *  @param colour  The colour to set
 */
extern void display_set_pixel(uint32_t x, uint32_t y, display_colour_t colour);

/** @brief Add two display regions together
 *  @param r1   The first region
 *  @param r2   The second region
 *  @returns   The encompassing region
 */
extern display_render_region_t display_render_region_add(display_render_region_t r1, display_render_region_t r2);

/** @brief Renders to the display
 *  @param region  The region to render (smaller = faster)
 *  @param render  The renderlist to draw from
 *  @returns  true
 */
extern bool display_render_(display_render_region_t region, display_shader_null_t* render);
#define display_render(region, render) (display_render_((region), (display_shader_null_t*)(render)))

#endif /* DRIVER_HILEVEL_H_ */

/*
 * command.c
 *
 * ST7735S Command implementations.
 */

#include "command.h"
#include "core.h"

void display_command_nop(void)
{
    display_write_command(DISPLAY_CMD_NOP);
}

void display_command_swreset(void)
{
    display_write_command(DISPLAY_CMD_SWRESET);
    *((uint32_t*)&display_status) = ((uint32_t)0x00010000) | (*((uint32_t*)&display_status) & ((uint32_t)0x7C700000));
}

void display_command_slpin(void)
{
    display_write_command(DISPLAY_CMD_SLPIN);
    display_status.SLP = DISPLAY_SLP_IN;

    __delay_cycles((DISPLAY_CLOCKRATE / 1000U) * 150U);
}

void display_command_slpout(void)
{
    display_write_command(DISPLAY_CMD_SLPOUT);
    display_status.SLP = DISPLAY_SLP_OUT;

    __delay_cycles((DISPLAY_CLOCKRATE / 1000U) * 150U);
}

void display_command_ptlon(void)
{
    display_write_command(DISPLAY_CMD_PTLON);
    display_status.PTL = DISPLAY_PTL_ON;
    display_status.NOR = DISPLAY_NOR_OFF;
}

void display_command_noron(void)
{
    display_write_command(DISPLAY_CMD_NORON);
    display_status.PTL = DISPLAY_PTL_OFF;
    display_status.NOR = DISPLAY_NOR_ON;
}

void display_command_ivnoff(void)
{
    display_write_command(DISPLAY_CMD_IVNOFF);
    display_status.INV = DISPLAY_INV_OFF;
}

void display_command_ivnon(void)
{
    display_write_command(DISPLAY_CMD_IVNON);
    display_status.INV = DISPLAY_INV_ON;
}

void display_command_gamset(uint8_t gc)
{
    display_write_command(DISPLAY_CMD_GAMSET);
    display_write_single_8(gc);
    display_status.GCSEL = gc;
}

void display_command_dispoff(void)
{
    display_write_command(DISPLAY_CMD_DISPOFF);
    display_status.DIS = DISPLAY_DIS_OFF;
}

void display_command_dispon(void)
{
    display_write_command(DISPLAY_CMD_DISPON);
    display_status.DIS = DISPLAY_DIS_ON;
}

void display_command_caset(uint16_t xs, uint16_t xe)
{
    display_write_command(DISPLAY_CMD_CASET);
    display_write_single_16(xs);
    display_write_single_16(xe);
}

void display_command_raset(uint16_t ys, uint16_t ye)
{
    display_write_command(DISPLAY_CMD_RASET);
    display_write_single_16(ys);
    display_write_single_16(ye);
}

void display_command_ramwr(void)
{
    display_write_command(DISPLAY_CMD_RAMWR);
}

void display_command_rgbset(void)
{
    display_write_command(DISPLAY_CMD_RGBSET);
}

void display_command_ptlar(uint16_t psl, uint16_t pel)
{
    display_write_command(DISPLAY_CMD_PTLAR);
    display_write_single_16(psl);
    display_write_single_16(pel);
}

void display_command_scrlar(uint16_t tfa, uint16_t vsa, uint16_t bfa)
{
    display_write_command(DISPLAY_CMD_SCRLAR);
    display_write_single_16(tfa);
    display_write_single_16(vsa);
    display_write_single_16(bfa);
}

void display_command_teoff(void)
{
    display_write_command(DISPLAY_CMD_TEOFF);
    display_status.TE = DISPLAY_TE_OFF;
}

void display_command_teon(void)
{
    display_write_command(DISPLAY_CMD_TEON);
    display_status.TE = DISPLAY_TE_ON;
}

void display_command_colmod(uint8_t ifpf)
{
    display_write_command(DISPLAY_CMD_COLMOD);
    display_write_single_8(ifpf);
}

void display_command_madctl(display_madctl_t ctl)
{
    display_write_command(DISPLAY_CMD_MADCTL);
    display_write_single_8(*((uint8_t*)&ctl));
}

void display_command_vscsad(uint8_t ssa)
{
    display_write_command(DISPLAY_CMD_VSCSAD);
    display_write_single_8(0x00U);
    display_write_single_8(ssa);
}

void display_command_idmoff(void)
{
   display_write_command(DISPLAY_CMD_IDMOFF);
   display_status.IDM = DISPLAY_IDM_OFF;
}

void display_command_idmon(void)
{
   display_write_command(DISPLAY_CMD_IDMON);
   display_status.IDM = DISPLAY_IDM_ON;
}

void display_command_frmctr1(uint8_t rtna, uint8_t fpa, uint8_t bpa)
{
    display_write_command(DISPLAY_CMD_FRMCTR1);
    display_write_single_8(rtna);
    display_write_single_8(fpa);
    display_write_single_8(bpa);
}

void display_command_frmctr2(uint8_t rtnb, uint8_t fpb, uint8_t bpb)
{
    display_write_command(DISPLAY_CMD_FRMCTR2);
    display_write_single_8(rtnb);
    display_write_single_8(fpb);
    display_write_single_8(bpb);
}

void display_command_frmctr3(uint8_t rtnc, uint8_t fpc, uint8_t bpc, uint8_t rtnd, uint8_t fpd, uint8_t bpd)
{
    display_write_command(DISPLAY_CMD_FRMCTR3);
    display_write_single_8(rtnc);
    display_write_single_8(fpc);
    display_write_single_8(bpc);
    display_write_single_8(rtnd);
    display_write_single_8(fpd);
    display_write_single_8(bpd);
}

void display_command_invctr(display_invctr_t ctl)
{
    display_write_command(DISPLAY_CMD_INVCTR);
    display_write_single_8(*((uint8_t*)&ctl));
}

void display_command_pwctr1(display_pwctr1_t power)
{
    display_write_command(DISPLAY_CMD_PWCTR1);

    power.padding1 = ((uint32_t)0);
    power.padding2 = ((uint32_t)1);

    display_write_single_16(*((uint16_t*)&power));
    display_write_single_8 (*(( uint8_t*)&power) + 2U);
}

void display_command_pwctr2(display_pwctr2_t power)
{
    display_write_command(DISPLAY_CMD_PWCTR2);
    display_write_single_8 (*(( uint8_t*)&power));
}

void display_command_pwctr3(display_pwctrn_t power)
{
    display_write_command(DISPLAY_CMD_PWCTR3);
    display_write_single_16(*((uint16_t*)&power));
}

void display_command_pwctr4(display_pwctrn_t power)
{
    display_write_command(DISPLAY_CMD_PWCTR4);
    display_write_single_16(*((uint16_t*)&power));
}

void display_command_pwctr5(display_pwctrn_t power)
{
    display_write_command(DISPLAY_CMD_PWCTR5);
    display_write_single_16(*((uint16_t*)&power));
}

void display_command_vmctr1(uint8_t vcoms)
{
    display_write_command(DISPLAY_CMD_VMCTR1);
    display_write_single_8(vcoms);
}

void display_command_vmofctr(uint8_t vmf)
{
    display_write_command(DISPLAY_CMD_VMOFCTR);
    display_write_single_8(vmf);
}

void display_command_wrid2(uint8_t id2)
{
    display_write_command(DISPLAY_CMD_WRID3);
    display_write_single_8(id2);
}

void display_command_wrid3(uint8_t id3)
{
    display_write_command(DISPLAY_CMD_WRID3);
    display_write_single_8(id3);
}

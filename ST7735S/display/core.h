/*
 * <display/core.h>
 *
 * Display driver core
 */

#ifndef DRIVER_CORE_H_
#define DRIVER_CORE_H_

#include "types.h"

#pragma CHECK_MISRA("none")
#include <stdbool.h>
#include <stdint.h>
#pragma RESET_MISRA("all")

/* true and false defs in stdbool.h are not MISRA compliant, so we define
 * these...
 */
#define _true  (1U)
#define _false (0U)

extern display_status_t display_status;

/* Set equal to __SYSTEM_CLOCK */
#define DISPLAY_CLOCKRATE 1500000U

/* DMA Config */
#define DMA_EUSCI_B0_TX0 0x02U
#define DISPLAY_DMA_CHANNEL 0U
#define DISPLAY_DMA_CHANNEL_BIT ((uint32_t)1U << DISPLAY_DMA_CHANNEL)

/** @brief Initialize driver
 */
void display_core_init(void);

/** @brief Write 8 bit value to display
 *  @param src   The 8 bit value to write
 *  @returns  Whether operation has completed.
 */
bool display_write_single_8(uint8_t src);

/** @brief Write 16 bit value to display
 *  @param src   The 16 bit value to write
 *  @returns  Whether operation has completed.
 */
bool display_write_single_16(uint16_t src);

/** @brief Write n byte value to display
 *  @param src   Pointer to the first byte.
 *  @param len   (Length of data) - 1
 *  @returns  Whether operation has completed.
 */
bool display_write_multiple(uint8_t* src, uint32_t len);

/** @brief Write a command to display
 *  @param command Command to write.
 *  @returns  Whether operation has completed.
 */
bool display_write_command(uint8_t command);

/** @brief Block until all write operations are finished
 */
void display_flush(void);

#endif /* DRIVER_CORE_H_ */

/*
 * hilevel.c
 *
 * ST7735S High-level implementation
 */

#include "hilevel.h"
#include "command.h"

#include "./shader/rect.h"

bool display_init(void)
{
   display_pwctr1_t power = {0};
   power.AVDD  = DISPLAY_AVDD_4V5;
   power.VRHP  = 0x0AU;
   power.VRHN  = 0x00U;
   power.MODE  = 0x00U;
   power.VRHN5 = 0x00U;
   power.VRHP5 = 0x00U;

   display_madctl_t mad = {0};
   mad.padding = 0U;
   mad.MY  = DISPLAY_MY_DECREMENT;
   mad.MX  = DISPLAY_MX_DECREMENT;
   mad.MV  = DISPLAY_MV_NORMAL;
   mad.ML  = DISPLAY_ML_DECREMENT;
   mad.RGB = DISPLAY_RGB_RGB;
   mad.MH  = DISPLAY_MH_DECREMENT;

   display_core_init();

   display_command_slpout();
   display_command_pwctr1(power);
   display_command_colmod(DISPLAY_IFPF_16);
   display_command_madctl(mad);

   /* turn on the display */
   display_command_dispon();

   return _true;
}

uint8_t display_min(uint8_t a, uint8_t b)
{
    uint8_t result = ((a) > (b)) ? b : a;
    return result;
}

uint8_t display_max(uint8_t a, uint8_t b)
{
    uint8_t result = ((a) > (b)) ? a : b;
    return result;
}

void display_clear(display_colour_t colour)
{
    display_shader_rect_t rect = {0};
    display_shader_set_next(&rect, NULL);
    display_render_region_t region = display_shader_rect_new(&rect, 0U, 0U, 127U, 127U);
    display_shader_rect_set_colour(&rect, colour);
    display_render(region, &rect);
}

void display_set_pixel(uint32_t x, uint32_t y, display_colour_t colour)
{
    display_command_caset(((uint16_t)x) + DISPLAY_OFFSET_X, ((uint16_t)x) + DISPLAY_OFFSET_X);
    display_command_raset(((uint16_t)y) + DISPLAY_OFFSET_Y, ((uint16_t)y) + DISPLAY_OFFSET_Y);
    display_command_ramwr();
    display_write_single_16(*((uint16_t*)&colour));
}

void display_shader_set_pixel(uint8_t buf[], uint32_t x, display_colour_t colour)
{
    x *= 2U;
    buf[x + 1U] = *(uint8_t*)&colour;
    buf[x + 0U] = (uint8_t)(((*(uint16_t*)&colour) & 0xFF00U) >> 8U);
}

display_colour_t display_shader_get_pixel(uint8_t buf[], uint32_t x)
{
    x *= 2U;
    uint8_t result[2] = {buf[x + 1U], buf[x + 0U]};
    return *(display_colour_t*)result;
}

bool display_shader_set_line(uint8_t* buf, uint32_t startx, uint32_t endx, display_colour_t colour)
{
    endx = endx - startx;
    buf += (startx * 2U);

    while(endx)
    {
        display_shader_set_pixel(buf, 0U, colour);
        buf += 2;
        endx --;
    }

    return _true;
}

void display_shader_set_next_(display_shader_null_t* render, display_shader_null_t* next)
    { render->next = next; }

void display_shader_set_shader_(display_shader_null_t* render, display_shader_t shader)
    { render->shader = shader; }

display_render_region_t display_render_region_add(display_render_region_t r1, display_render_region_t r2)
{
    display_render_region_t result = {0};

    result.x1 = display_min(r1.x1, r2.x1);
    result.y1 = display_min(r1.y1, r2.y1);
    result.x2 = display_max(r1.x2, r2.x2);
    result.y2 = display_max(r1.y2, r2.y2);
    return result;
}

bool display_render_(display_render_region_t region, display_shader_null_t* render)
{
    /* This buf stores buffers for the DMA */
    static uint8_t bufstor[DISPLAY_BUFFER_SIZE * DISPLAY_NUM_BUFFERS] = {0};

    /* set x and y range */
    const uint16_t x1 = region.x1;
    const uint16_t x2 = region.x2;
    const uint16_t y1 = region.y1;
    const uint16_t y2 = region.y2;
    display_command_caset(x1 + DISPLAY_OFFSET_X, x2 + DISPLAY_OFFSET_X);
    display_command_raset(y1 + DISPLAY_OFFSET_Y, y2 + DISPLAY_OFFSET_Y);
    display_command_ramwr();

    memset(bufstor, 0x00, DISPLAY_BUFFER_SIZE);

    uint32_t w = ((region.x2 - region.x1) + 1U) * 2U;

    uint32_t lineTotal = DISPLAY_BUFFER_SIZE / w;
    uint32_t lineAt    = 0;
    uint32_t blockAt   = 0;

    for(uint32_t i = y1; (uint8_t)i <= region.y2; i++)
    {
        display_shader_null_t* walk = render;
        while((walk != NULL) && (walk->shader != NULL))
        {
            display_shader_null_t* walk2 = walk; /* This shall appease you, Mr. Compiler. */
            walk->shader(walk2, region, &bufstor[(blockAt * DISPLAY_BUFFER_SIZE) + (lineAt * w)], i);
            walk = walk->next;
        }

        lineAt = (lineAt + 1U) % lineTotal;

        if(lineAt == 0U)
        {
            display_write_multiple(&bufstor[blockAt * DISPLAY_BUFFER_SIZE], (lineTotal * w) - 1U);
            blockAt = (blockAt + 1U) % DISPLAY_NUM_BUFFERS;
            memset(&bufstor[blockAt * DISPLAY_BUFFER_SIZE], 0x00, DISPLAY_BUFFER_SIZE);
        }
    }

    /* Write out last buffer */
    if(lineAt)
    {
        display_write_multiple(&bufstor[blockAt * DISPLAY_BUFFER_SIZE], lineAt * w);
    }

    return _true;
}

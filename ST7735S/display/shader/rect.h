/*
 * "display/shader/rect.h"
 *
 * ST7735S High-level text shader
 */

#ifndef DISPLAY_SHADER_RECT_H_
#define DISPLAY_SHADER_RECT_H_

#include "../display.h"

typedef struct display_shader_rect_t_
{
    display_shader_null_t base;

    display_render_region_t region;
    display_colour_t colour;

} display_shader_rect_t;

/** Rectangle Shader */
extern void display_shader_rect(void* data, display_render_region_t region, uint8_t* buf, uint32_t i);

/** @brief Creates a text object
 *  @param tri  The text object to create
 *  @param x1   X position of the top-left corner
 *  @param y1   Y position of the top-left corner
 *  @param x2   X position of the bottom-right corner
 *  @param y2   Y position of the bottom-right corner
 */
extern display_render_region_t display_shader_rect_new(display_shader_rect_t* rect, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2);

/** @brief Sets the colour of a rectangle
 *  @param rect   The rect to set the colour for
 *  @param colour The colour to set
 */
extern void display_shader_rect_set_colour(display_shader_rect_t* rect, display_colour_t colour);

/** @brief Gets the colour of a rectangle
 *  @param rect   The rect to get the colour of
 *  @returns      The current colour
 */
extern display_colour_t display_shader_rect_get_colour(display_shader_rect_t* rect);

#endif /* DISPLAY_SHADER_RECT_H_ */

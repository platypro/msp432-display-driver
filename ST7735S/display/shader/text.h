/*
 * shader/text.h
 *
 * ST7735S High-level text shader
 */

#ifndef INC_DISPLAY_SHADER_TEXT_H_
#define INC_DISPLAY_SHADER_TEXT_H_

#include "display/hilevel.h"

typedef struct display_shader_text_t_
{
    display_shader_null_t base;

    uint8_t* text;
    uint8_t ystart;
    uint8_t xstart;

    display_colour_t colour;

} display_shader_text_t;

/** Triangle Shader */
extern void display_shader_text(void* data, display_render_region_t region, uint8_t* buf, uint32_t i);

/** @brief Creates a text object
 *  @param tri  The text object to create
 *  @param x    X position of the text
 *  @param y    Y position of the text
 *  @param anchor  The alignment of the text
 *  @returns   A region containing just the text
 */
extern display_render_region_t display_shader_text_new(display_shader_text_t* text, char* str, uint32_t x, uint32_t y, display_anchor_t anchor);

/** @brief Sets the colour of a text object
 *  @param text   The text to set the colour for
 *  @param colour The colour to set
 */
extern inline void display_shader_text_set_colour(display_shader_text_t* text, display_colour_t colour);

/** @brief Gets the colour of a text object
 *  @param text   The text to get the colour of
 *  @returns      The current colour
 */
extern inline display_colour_t display_shader_text_get_colour(display_shader_text_t* text);

#endif /* INC_DISPLAY_SHADER_TEXT_H_ */

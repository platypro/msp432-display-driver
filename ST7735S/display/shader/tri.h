/*
 * shader/tri.h
 *
 * ST7735S High-level triangle shader
 */

#ifndef INC_DISPLAY_SHADER_TRI_H_
#define INC_DISPLAY_SHADER_TRI_H_

#include "display/hilevel.h"

typedef struct display_shader_tri_t_
{
    display_shader_null_t base;

    uint8_t btmy; /* Bottom y coordinate */
    uint8_t midy; /* Middle y coordinate */
    uint8_t topy; /* Top y coordinate */

    float slopelong; /* Slope of long side */
    float slopelow;  /* Slope of lower short side */
    float slopehigh; /* Slope of upper short side */

    float baseX;     /* Starting X value for draw*/

    display_colour_t colour;

} display_shader_tri_t;

/** Triangle shader */
extern void display_shader_tri(void* data, display_render_region_t region, uint8_t* buf, uint32_t i);

/** @brief Creates a triangle object
 *  @param tri  The triangle object to create
 *  @param x1   X Value for point 1
 *  @param y1   Y Value for point 1
 *  @param x2   X Value for point 2
 *  @param y2   Y Value for point 2
 *  @param x3   X Value for point 3
 *  @param y3   Y Value for point 3
 *  @returns   A region containing just the triangle
 */
extern display_render_region_t display_shader_tri_new(display_shader_tri_t* tri, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t x3, uint32_t y3);

/** @brief Sets the colour of a triangle object
 *  @param tri    The tri to set the colour for
 *  @param colour The colour to set
 */
extern void display_shader_tri_set_colour(display_shader_tri_t* tri, display_colour_t colour);

/** @brief Gets the colour of a triangle object
 *  @param tri    The tri to get the colour of
 *  @returns      The current colour
 */
extern display_colour_t display_shader_tri_get_colour(display_shader_tri_t* tri);

#endif /* INC_DISPLAY_SHADER_TRI_H_ */

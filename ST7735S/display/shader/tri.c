/*
 * shader/tri.c
 *
 * ST7735S High-level triangle shader implementation
 */

#include "display/shader/tri.h"

inline void swap(uint32_t* x, uint32_t* y);

inline void swap(uint32_t* x, uint32_t* y)
{
    *x = (*x)+(*y);
    *y = (*x)-(*y);
    *x = (*x)-(*y);
}

void display_shader_tri(void* data, display_render_region_t region, uint8_t* buf, uint32_t i)
{
    display_shader_tri_t* tri = (display_shader_tri_t*) data;
    if(((uint8_t)i < tri->topy) && ((uint8_t)i > tri->btmy))
    {
        uint32_t draw1 = tri->baseX;
        uint32_t draw2 = tri->baseX;

        /* Temp variables to appease Mr. Compiler */
        const float btmy = tri->btmy;
        const float midy = tri->midy;

        draw1 += (uint32_t)lrintf(tri->slopelong * ((float)i - btmy));

        if((uint8_t)i > tri->midy)
        {
            draw2 += (uint32_t)lrintf(tri->slopelow  * (midy - btmy));
            draw2 += (uint32_t)lrintf(tri->slopehigh * ((float)i - midy));
        }
        else { draw2 += (uint32_t)lrintf(tri->slopelow * ((float)i - btmy)); }

        if(draw1 > draw2) { swap((uint32_t*)&draw1, (uint32_t*)&draw2); }

        const uint32_t x1 = region.x1;
        const uint32_t x2 = region.x2;

        if(!(((uint8_t)draw1 > region.x2) || ((uint8_t)draw2 < region.x1)))
        {
            /* Triange is on screen */
            draw1 = display_max((uint8_t)draw1, (uint8_t)x1);
            draw2 = display_min((uint8_t)draw2, (uint8_t)x2);

            display_shader_set_line(buf, draw1 - x1, draw2 - x1, tri->colour);
        }
    }
}

display_render_region_t display_shader_tri_new(display_shader_tri_t* tri, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint32_t x3, uint32_t y3)
{
    display_shader_set_shader(tri, &display_shader_tri);

    if (y1 > y2) { swap(&y1, &y2); swap(&x1, &x2); }
    if (y2 > y3) { swap(&y2, &y3); swap(&x2, &x3); }
    if (y1 > y2) { swap(&y1, &y2); swap(&x1, &x2); }

    tri->topy = ((uint8_t)y3);
    tri->midy = ((uint8_t)y2);
    tri->btmy = ((uint8_t)y1);

    if(y3 != y1)
        tri->slopelong = ((float)x3 - (float)x1) / ((float)y3 - (float)y1);

    if(y2 != y1)
        tri->slopelow  = ((float)x2 - (float)x1) / ((float)y2 - (float)y1);
    else
    {
        tri->slopelow  = (float)x2 - (float)x1;
        tri->midy ++;
    }

    if(y3 != y2)
        tri->slopehigh = ((float)x3 - (float)x2) / ((float)y3 - (float)y2);

    tri->baseX  = (float)x1;

    display_render_region_t result;
    result.x1 = (display_min(display_min((uint8_t)x1,(uint8_t)x2),(uint8_t)x3));
    result.y1 = ((uint8_t)y1);
    result.x2 = (display_max(display_max((uint8_t)x1,(uint8_t)x2),(uint8_t)x3));
    result.y2 = ((uint8_t)y3);
    return result;
}

void display_shader_tri_set_colour(display_shader_tri_t* tri, display_colour_t colour)
    { tri->colour = colour; }

display_colour_t display_shader_tri_get_colour(display_shader_tri_t* tri)
    { return tri->colour; }


/*
 * shader/text.c
 *
 * ST7735S High-level text shader implementation
 */

#include "display/shader/text.h"
#include "display/font.h"

void display_shader_text(void* data, display_render_region_t region, uint8_t* buf, uint32_t i)
{
    display_shader_text_t* text = (display_shader_text_t*) data;

/* We are looking up string values here using array lookups.
 */
#pragma CHECK_MISRA("-17.4")
    const uint32_t ystart = text->ystart;
    if((i >= ystart) && (i < (ystart + FONT_HEIGHT)))
    {
        uint8_t* c = text->text;
        const uint16_t x2 = region.x2;
        uint32_t xval = text->xstart - region.x1;
        uint8_t shift = (uint32_t)1U << (i - ystart);

        while((*c != 0U) && (xval < x2))
        {
            uint32_t j = (x2 - xval);
            j = (FONT_WIDTH > j) ? j : FONT_WIDTH;

            while(j)
            {
                j--;
                if(font[(*c) - 32U][j] & shift)
                {
                    display_shader_set_pixel(buf, xval + j, text->colour);
                    display_colour_t testColour = display_shader_get_pixel(buf, xval + j);
                }
            }
            c++;
            xval += FONT_WIDTH + FONT_SPACING;
        }
    }
#pragma RESET_MISRA("17.4")
}

display_render_region_t display_shader_text_new(display_shader_text_t* text, char* str, uint32_t x, uint32_t y, display_anchor_t anchor)
{
    display_render_region_t result;
    display_shader_set_shader(text, &display_shader_text);
    text->text = (uint8_t*)str;
    text->colour.r = 31U;
    text->colour.g = 63U;
    text->colour.b = 31U;

    result.x2 = (uint8_t)((strlen(str) * (FONT_WIDTH + FONT_SPACING)) - FONT_SPACING);

    switch(anchor & DISPLAY_ANCHOR_MASK)
    {
    case DISPLAY_ANCHOR_FIRST:
    {
        text->ystart = (uint8_t)y;
        break;
    }
    case DISPLAY_ANCHOR_MIDDLE:
    {
        text->ystart = (uint8_t)((y > (FONT_HEIGHT / 2U)) ? y - (FONT_HEIGHT / 2U) : 0U);
        break;
    }
    case DISPLAY_ANCHOR_LAST:
    {
        text->ystart = (uint8_t)((y > FONT_HEIGHT) ? y - FONT_HEIGHT + 1U : 0U);
        break;
    }
    default: break;
    }

    switch((uint32_t)(anchor & ((uint32_t)DISPLAY_ANCHOR_MASK << DISPLAY_ANCHOR_HORI_SHIFT)) >> DISPLAY_ANCHOR_HORI_SHIFT)
    {
    case DISPLAY_ANCHOR_FIRST:
    {
        text->xstart = (uint8_t)x;
        break;
    }
    case DISPLAY_ANCHOR_MIDDLE:
    {
        const uint32_t x2 = result.x2;
        text->xstart = (uint8_t)((x > (x2 / 2U)) ? x - (x2 / 2U) : 0U);
        break;
    }
    case DISPLAY_ANCHOR_LAST:
    {
        const uint32_t x2 = result.x2;
        text->xstart = (uint8_t)((x > x2) ? x - x2 : 0U);
        break;
    }
    default: break;
    }

    result.y1 = text->ystart;
    result.x1 = text->xstart;
    result.x2 += text->xstart;
    result.y2 = FONT_HEIGHT - 1U;
    result.y2 += text->ystart;

    return result;
}

void display_shader_text_set_colour(display_shader_text_t* text, display_colour_t colour)
    { text->colour = colour; }

display_colour_t display_shader_text_get_colour(display_shader_text_t* text)
    { return text->colour; }

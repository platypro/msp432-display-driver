#include "rect.h"

void display_shader_rect(void* data, display_render_region_t region, uint8_t* buf, uint32_t i)
{
    display_shader_rect_t* rect = (display_shader_rect_t*) data;

    if(((uint8_t)i > rect->region.y1) && ((uint8_t)i < rect->region.y2))
    {
        uint32_t xstart = display_max(rect->region.x1, region.x1) - rect->region.x1;
        uint32_t xend = display_min(rect->region.x2, region.x2) - rect->region.x1;
        display_shader_set_line(buf, xstart, xend, rect->colour);
    }
}

display_render_region_t display_shader_rect_new(display_shader_rect_t* rect, uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2)
{
    display_shader_set_shader(rect, &display_shader_rect);
    rect->region.x1 = ((uint8_t)x1);
    rect->region.y1 = ((uint8_t)y1);
    rect->region.x2 = ((uint8_t)x2);
    rect->region.y2 = ((uint8_t)y2);
    return rect->region;
}

void display_shader_rect_set_colour(display_shader_rect_t* rect, display_colour_t colour)
    { rect->colour = colour; }

display_colour_t display_shader_rect_get_colour(display_shader_rect_t* rect)
    { return rect->colour; }

#pragma CHECK_MISRA("none")
#include "msp.h"
#include <stdint.h>
#pragma RESET_MISRA("all")

#include "display/display.h"
#include "display/shader/text.h"
#include "display/shader/tri.h"

#include "clock.h"
#include "dem_trispin.h"
#include "dem_text.h"

#if DISPLAY_CLOCKRATE == 1500000
#define SYS_CLOCK_TEXT "1.5MHz"
#elif DISPLAY_CLOCKRATE == 3000000
#define SYS_CLOCK_TEXT "3.0MHz"
#elif DISPLAY_CLOCKRATE == 48000000
#define SYS_CLOCK_TEXT "48.0MHz"
#else
#define SYS_CLOCK_TEXT "??MHz"
#endif

#define DEBOUNCE_LEN 5
#define PORT_BUTTON1 P5
#define PIN_BUTTON1  BIT1

#define TICK_COUNTDOWN_MAX 5

#define NUMELEMENTS(array) (sizeof(array) / sizeof(*array))

volatile bool require_draw = false;
volatile bool next_subprogram = false;
volatile uint32_t button1_debounce = 0;
volatile uint32_t ticks_since = 0;

union staticVars {
    dem_trispin_t spin;
    dem_text_t text;
} vars;

struct sub_program_t {
    char* name;
    void (*init) (void* data_, struct display_render_region_t_ base_region);
    uint32_t (*update) (void* data_, uint32_t ticks);
    void (*draw) (void* data_);
} subprograms[] = {
   {"Triangle Spin", dem_trispin_init, dem_trispin_update, dem_trispin_draw },
   {"Text Demo", dem_text_init, dem_text_update, dem_text_draw },
};

void SysTick_Handler(void)
{
    if(require_draw == false)
        require_draw = true;

    if(button1_debounce) button1_debounce --;

    ticks_since ++;
}

void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

	// Enable Button 1 with interrupt
    PORT_BUTTON1->DIR &= ~PIN_BUTTON1;
    PORT_BUTTON1->OUT |=  PIN_BUTTON1;
    PORT_BUTTON1->REN |=  PIN_BUTTON1;
    PORT_BUTTON1->IE  |=  PIN_BUTTON1;
    PORT_BUTTON1->IES |=  PIN_BUTTON1;

    NVIC_EnableIRQ(PORT5_IRQn);

	// Set up systick for every 20th of a second
	SysTick_Config(SystemCoreClock / 20);

	display_init();
    clock_setup();

    /* System load setup and render region setup */
    display_render_region_t render_region;
    display_render_region_t timetext_region;
    display_shader_text_t textop1, textop2;
    display_shader_set_next(&textop2, NULL);
    display_shader_set_next(&textop1, &textop2);

    render_region = display_shader_text_new(&textop1, "Load:", 0U, 0U, DISPLAY_ANCHOR_TOP_LEFT);
    display_shader_text_set_colour(&textop1, DISPLAY_COLOUR_WHITE);
    timetext_region = display_shader_text_new(&textop2, SYS_CLOCK_TEXT, 127U, 0U, DISPLAY_ANCHOR_TOP_RIGHT);
    display_shader_text_set_colour(&textop2, DISPLAY_COLOUR_WHITE);

    timetext_region.x2 = timetext_region.x1 - 1U;
    timetext_region.x1 = render_region.x2;

    render_region.x2 = 127;

    display_render(render_region, (display_shader_null_t*)&textop1);

    render_region.y1 = render_region.y2;
    render_region.y2 = 127;

    display_shader_text_t sysloadpercent;
    sysloadpercent.base.shader = NULL;
    sysloadpercent.base.next = NULL;

    uint32_t tick_countdown = 0;
    uint32_t subprogram_at = 0;

    subprograms[subprogram_at].init(&vars, render_region);

	while(true)
	{
        if(next_subprogram)
        {
            subprogram_at = (subprogram_at + 1) % NUMELEMENTS(subprograms);
            subprograms[subprogram_at].init(&vars, render_region);
            next_subprogram = false;
        }

	    if(require_draw)
	    {
	        clock_start();
	        subprograms[subprogram_at].update(&vars, ticks_since);
	        ticks_since = 0;

	        if(!tick_countdown)
	        {
	            char timebuf[CLOCK_TEXT_TIME_MIN + 2];
	            timebuf[CLOCK_TEXT_TIME_MIN + 1] = '\000';
	            timebuf[CLOCK_TEXT_TIME_MIN]     = '%';
	            char* newtime = clock_text_time(timebuf + CLOCK_TEXT_TIME_MIN - 1) + 1;

	            display_shader_text_new(&sysloadpercent, newtime, timetext_region.x1, 0, DISPLAY_ANCHOR_TOP_LEFT);
	            display_render(timetext_region, (display_shader_null_t*)&sysloadpercent);
	            tick_countdown = TICK_COUNTDOWN_MAX;
	        }
	        tick_countdown --;

	        subprograms[subprogram_at].draw(&vars);
	        clock_end();

	        require_draw = false;
	    }

	    __WFE();
	}
}

void PORT5_IRQHandler(void)
{
  if(!button1_debounce)
      { next_subprogram = true; }
  PORT_BUTTON1->IFG &= ~PIN_BUTTON1;
  button1_debounce = DEBOUNCE_LEN;
  __DSB();
}

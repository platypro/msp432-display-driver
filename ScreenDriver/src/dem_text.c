#include "dem_text.h"

#include "display/display.h"
#include "display/shader/text.h"

void dem_text_init(void* data_, display_render_region_t base_region)
{
    display_shader_null_t null = {0};
    display_render(base_region, &null);

    display_shader_text_t text;
    text.base.shader = NULL;
    display_shader_set_next(&text, NULL);
    display_shader_text_set_colour(&text, DISPLAY_COLOUR_WHITE);

    display_render_region_t roll_region = base_region;

    roll_region = display_shader_text_new(&text, "TL", 0, base_region.y1, DISPLAY_ANCHOR_TOP_LEFT);
    display_render(roll_region, (display_shader_null_t*)&text);

    roll_region = display_shader_text_new(&text, "TC", 63, base_region.y1, DISPLAY_ANCHOR_TOP_CENTER);
    display_render(roll_region, (display_shader_null_t*)&text);

    roll_region = display_shader_text_new(&text, "TR", 127, base_region.y1, DISPLAY_ANCHOR_TOP_RIGHT);
    display_render(roll_region, (display_shader_null_t*)&text);

    roll_region = display_shader_text_new(&text, "ML", 0, 63, DISPLAY_ANCHOR_MIDDLE_LEFT);
    display_render(roll_region, (display_shader_null_t*)&text);

    roll_region = display_shader_text_new(&text, "MC", 63, 63, DISPLAY_ANCHOR_MIDDLE_CENTER);
    display_render(roll_region, (display_shader_null_t*)&text);

    roll_region = display_shader_text_new(&text, "MR", 127, 63, DISPLAY_ANCHOR_MIDDLE_RIGHT);
    display_render(roll_region, (display_shader_null_t*)&text);

    roll_region = display_shader_text_new(&text, "BL", 0, 127, DISPLAY_ANCHOR_BOTTOM_LEFT);
    display_render(roll_region, (display_shader_null_t*)&text);

    roll_region = display_shader_text_new(&text, "BC", 63, 127, DISPLAY_ANCHOR_BOTTOM_CENTER);
    display_render(roll_region, (display_shader_null_t*)&text);

    roll_region = display_shader_text_new(&text, "BR", 127, 127, DISPLAY_ANCHOR_BOTTOM_RIGHT);
    display_render(roll_region, (display_shader_null_t*)&text);
}

uint32_t dem_text_update(void* data_, uint32_t ticks)
{
    return 0;
}

void dem_text_draw(void* data_)
{

}

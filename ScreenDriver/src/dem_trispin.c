#pragma CHECK_MISRA("none")
#include <stdbool.h>
#include <math.h>
#pragma RESET_MISRA("all")

#include "display/display.h"
#include "display/shader/tri.h"
#include "dem_trispin.h"

void dem_trispin_init(void* data_, struct display_render_region_t_ base_region)
{
    dem_trispin_t* data = (dem_trispin_t*)data_;
    data->render_region = base_region;
    data->app_region_new = base_region;
//    data->triangle.base.shader = NULL;
}

uint32_t dem_trispin_update(void* data_, uint32_t ticks)
{
    dem_trispin_t* data = (dem_trispin_t*)data_;
    data->rot += ticks * (PI / 128);
    if(data->rot >= (2*PI)) { data->rot = 0; }
    return 0;
}

void dem_trispin_draw(void* data_)
{
    dem_trispin_t* data = (dem_trispin_t*)data_;
    display_render(data->render_region, (display_shader_null_t*)&data->triangle);

    data->render_region = data->app_region_new;

    uint32_t x1 = 64 - RADIUS * cos(data->rot);
    uint32_t x2 = 64 - RADIUS * cos(data->rot + THIRD_REV);
    uint32_t x3 = 64 - RADIUS * cos(data->rot - THIRD_REV);

    uint32_t y1 = 64 + RADIUS * sin(data->rot);
    uint32_t y2 = 64 + RADIUS * sin(data->rot + THIRD_REV);
    uint32_t y3 = 64 + RADIUS * sin(data->rot - THIRD_REV);

    data->app_region_new = display_shader_tri_new(&data->triangle, x1, y1, x2, y2, x3, y3);

    display_shader_tri_set_colour(&data->triangle, (display_colour_t){.r=31, .g=0, .b=31});
    display_shader_set_next(&data->triangle, NULL);

    data->render_region = display_render_region_add(data->render_region, data->app_region_new);
}

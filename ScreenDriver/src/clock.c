#pragma CHECK_MISRA("none")
#include "msp.h"
#pragma RESET_MISRA("all")

#include "clock.h"

uint32_t clock_time;

uint32_t ticks = 0;
uint32_t ticks_per = 0;
uint32_t ticks_len = 0;

void TA0_0_IRQHandler(void)
{
    ticks += ticks_per;
    TIMER_A0->CCTL[0] &= ~TIMER_A_CTL_IFG;
    __DSB();
}

void TA0_N_IRQHandler(void)
{
    clock_time = ticks + TIMER_A0->CCR[1] / ticks_len;
    TIMER_A0->CCTL[1] &= ~TIMER_A_CCTLN_CCIFG;
    __DSB();
}

void clock_setup()
{
    NVIC_EnableIRQ(TA0_N_IRQn);
    NVIC_EnableIRQ(TA0_0_IRQn);

    clock_time = 0;

    /* Num clock cycles per percentage load (SystemCoreClock / 20 / 100 / div)*/
    ticks_len = SystemCoreClock / 4000;

    TIMER_A0->CTL = TIMER_A_CTL_SSEL__SMCLK | TIMER_A_CTL_ID__2 | TIMER_A_CTL_MC__STOP;

    /* Set up compare timer */
    TIMER_A0->CCR[0] = (0xFFFF - (0xFFFF % ticks_len));
    TIMER_A0->CCTL[0] = TIMER_A_CCTLN_CCIE;

    ticks_per = TIMER_A0->CCR[0] / ticks_len;

    /* Set up Capture timer */
    TIMER_A0->CCTL[1] = TIMER_A_CCTLN_CM__BOTH | TIMER_A_CCTLN_CAP | TIMER_A_CCTLN_CCIE | TIMER_A_CCTLN_CCIS_2;
}

void clock_start()
{
    ticks = 0;

    /* Clear the timer */
    //TIMER_A0->CTL |= TIMER_A_CTL_CLR;
    TIMER_A0->R = 0;

    /* Start the timer */
    TIMER_A0->CTL &= ~TIMER_A_CTL_MC_MASK;
    TIMER_A0->CTL |= TIMER_A_CTL_MC__UP;
}

void clock_end()
{
    /* Capture time */
    TIMER_A0->CCTL[1] ^= TIMER_A_CCTLN_CCIS_1;

    /* Stop the timer */
    TIMER_A0->CTL &= ~TIMER_A_CTL_MC_MASK;
    TIMER_A0->CTL |= TIMER_A_CTL_MC__STOP;
}

char* clock_text_time(char* buf)
{
    uint32_t n = clock_time;

    /* Note here that this buffer is written to in reverse and read from in reverse. */

    if(!n) { buf[0] = '0'; buf--; }
    while(n)
    {
        buf[0] = (n % 10) + '0';
        n /= 10;
        buf--;
    }

    return buf;
}

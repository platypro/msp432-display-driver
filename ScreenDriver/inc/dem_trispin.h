#ifndef INC_DEM_TRISPIN_H_
#define INC_DEM_TRISPIN_H_

#pragma CHECK_MISRA("none")
#include <stdint.h>
#pragma RESET_MISRA("all")

#define RADIUS 25
#define PI 3.1415926535897

#define THIRD_REV 2.0943951 // 2 * PI / 3

struct display_shader_tri_t_;
struct display_render_region_t_;

typedef struct dem_trispin_t_
{
    float rot;

    struct display_render_region_t_ render_region;
    struct display_render_region_t_ app_region_new;
    struct display_shader_tri_t_ triangle;

} dem_trispin_t;

extern void dem_trispin_init(void* data_, struct display_render_region_t_ base_region);
extern uint32_t dem_trispin_update(void* data_, uint32_t ticks);
extern void dem_trispin_draw(void* data_);

#endif /* INC_DEM_TRISPIN_C_ */

/*
 * clock.h
 *
 * Program Timer
 */

#ifndef INC_CLOCK_H_
#define INC_CLOCK_H_

#pragma CHECK_MISRA("none")
#include <stdint.h>
#pragma RESET_MISRA("all")

extern uint32_t clock_time;

#define CLOCK_TEXT_TIME_MIN 11

void clock_setup();
void clock_start();
void clock_end();

/* Write the reversed clock time into buffer */
char* clock_text_time(char* buf);

#endif /* INC_CLOCK_H_ */

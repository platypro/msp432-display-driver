#ifndef INC_DEM_TEXT_H_
#define INC_DEM_TEXT_H_

#include "display/display.h"

#pragma CHECK_MISRA("none")
#include <stdint.h>
#pragma RESET_MISRA("all")

typedef struct dem_text_t_{} dem_text_t;

extern void dem_text_init(void* data_, display_render_region_t base_region);
extern uint32_t dem_text_update(void* data_, uint32_t ticks);
extern void dem_text_draw(void* data_);

#endif /* INC_DEM_TEXT_H_ */

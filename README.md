MSP432 Screen Driver
====================
Author: Aeden McClain (https://www.platypro.net/)  
Video Demo: https://youtu.be/UJ-_BHfr4og  

This is a *FAST* display driver for the MSP432 Launchpad + Educational Boosterpack with the ST7735S display driver. It uses DMA for transfers and has C bindings for almost all functions available for the ST7735S. Also included here is a demo app with a spinning triangle and a text test. The TI compiler is required and Code Composer Studio is recommended.

Use within your project
-----------------------
You have two options:

 - Copy the ST7735S project into your workspace, add `${workspace_loc:/ST7735S/Debug/ST7735S.lib}` to the library list for your project and `${workspace_loc:/ST7735S}` to the include paths for your project, then build the ST7735S project.
 - Copy the `display` directory from within `ST7735S` into the root of your project.

The first option is better since you will get the optimized version of the driver (which is close to twice as fast!), but is harder to get working. That said, the second option really is as simple as copying a directory.

How to Make things with it
--------------------------
Lets start with a complete beginner example:
```
#include "msp.h"
#include "display/display.h"

void main(void)
{
  /* Stop watchdog timer.
   */
  WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;

  /* Initialize the display. If you don't run this command, nothing will happen.
   */
  display_init();

  /* Clear the display with the colour black.
   */
  display_clear(DISPLAY_COLOUR_BLACK);

  /* Set the pixel in the middle of the screen to white. The display is 128x128 pixels, with (0,0) 
   * representing the top left corner of the screen and (127,127) representing the bottom right
   * corner.
   */
  display_set_pixel(64, 64, DISPLAY_COLOUR_WHITE);

  /* Loop forever.
   */
  while(true)
    { __WFE(); }
}
```
This is an easy example to understand. We initialize the display, clear it to black, then set a single pixel to the colour white. This code is simple, but code written in this style will run slowly. The MSP432 contains a piece of hardware called DMA which can send data to the display in the background and at a very fast speed. With this style, the processor is completely responsible for telling the display what pixel to set and where. This does not scale since the DMA never has a chance to take over and send batch data to the display. The following example shows the proper way to draw to the screen:
```
#include "msp.h"
#include "display/display.h"
#include "display/shader/rect.h"
#include "display/shader/text.h"

void main(void)
{
  /* Stop watchdog timer.
   */
  WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;

  /* Initialize the display. If you don't run this command, nothing will happen.
   */
  display_init();

  /* Create the text shader object
   */
  display_shader_text_t myText;
  display_shader_text_new(&myText, "Display Test", 40, 40, DISPLAY_ANCHOR_TOP_LEFT);
  display_shader_text_set_colour(&myText, DISPLAY_COLOUR_GREEN);
  display_shader_set_next(&myText, NULL);

  /* Create the rectangle shader object
   */
  display_shader_rect_t myRect;
  display_shader_rect_new(&myRect, 20, 20, 50, 50);
  display_shader_rect_set_colour(&myRect, DISPLAY_COLOUR_WHITE);
  display_shader_set_next(&myRect, &myText);

  /* Render the image to the full render region
   */
  display_render(DISPLAY_RENDER_REGION_FULL, &myRect);

  /* Loop forever.
   */
  while(true)
    { __WFE(); }
}
```
The main thing here is the introduction of shaders. Shaders are objects which render various objects to the display. The driver contains some built-in shaders under the `display/shader` directory. In this example we create two shader objects, a rectangle shader and a text shader. The typical pattern for initializing objects involves creating the object, calling its corresponding "new" function with parameters, then setting further properties. The "new" function for a shader is responsible for handling all calculations needed before rendering and sets the correct fields in the shader object. Every shader object also has a property called `next` which allows you to render multiple overlapping shaders to the screen at once using a linked-list. In this case we set the `next` property of the rectangle shader to the text object, then the `next` property of the text shader to NULL. This tells the driver to render the rectangle with the text on top of it, then stop.

The next thing introduced here are render regions. To make things faster, the program could keep track of where new things are drawn and then only render the parts of the screen which changed. The display starts with random image data, so in this case we want to render the entire screen. We do this by choosing `DISPLAY_RENDER_REGION_FULL` to be our render region.

Finally, we render everything by passing the render region and the first shader to `display_render`. This way of controlling the display is a lot faster and more powerful than setting each pixel individually.

Defining Colours
----------------
Colours in this driver have three parts: a red part, a green part, and a blue part. A C structure named `display_colour_t` is available which contains fields for all of these parts. Following is a colour definition for the colour white:

```
display_colour_t colour = {
  .r = 31
  .g = 63
  .b = 31
}
```

The syntax used here creates a new object of type `display_colour_t` with `r`, `g`, and `b` fields set. Note the colour values here. White is a colour with all of these values maxed out. This display driver uses 5 bits of precision for red, 6 for green, and 5 for blue. Since `31 = 11111b` and `63 = 111111b` this will get us the colour white.

Working With Render Regions
---------------------------
A render region represents a portion of the screen. It is convention for shader "new" functions to return the minimal render region to draw the object. The typical workflow is to call "new" for all of your objects, then combine the resulting regions with `display_render_region_add`:
```
  /* Create the text shader object
   */
  display_shader_text_t myText;
  display_render_region_t myTextRegion;
  myTextRegion = display_shader_text_new(&myText, "Display Test", 40, 40, DISPLAY_ANCHOR_TOP_LEFT);
  display_shader_text_set_colour(&myText, DISPLAY_COLOUR_GREEN);
  display_shader_set_next(&myText, NULL);

  /* Create the rectangle shader object
   */
  display_shader_rect_t myRect;
  display_render_region_t myRectRegion;
  myRectRegion = display_shader_rect_new(&myRect, 20, 20, 50, 50);
  display_shader_rect_set_colour(&myRect, DISPLAY_COLOUR_WHITE);
  display_shader_set_next(&myRect, &myText);

  display_render(display_render_region_add(myRectRegion, myTextRegion), &myRect);
```
This will render to a region big enough to cover `myRect` and `myText` but no larger. Note however that this entire area gets rendered to, so any existing data on that part of the screen will be overwritten.

Creating Shaders
----------------
Shaders are objects which draw things to the screen. The first step in defining a shader is the data structure. In the following example we create a structure for a shader named `display_shader_test`:
```
typedef struct display_shader_test_t_
{
    display_shader_null_t base;

    /* Shader fields go here */

} display_shader_test_t;
```
The most important part of defining this struct is that its first element is of type `display_shader_null_t`. This contains fields which must be implemented by all shaders, such as the `next` field. The rest of the struct contains all of the other information your specific shader needs. It is good practice to choose fields for this structure with values that do not change over the course of drawing, i.e. it is immutable. This allows for the same shader object to be rendered multiple times without having to reset anything manually.

Next up is the shader function:
```
void display_shader_test(void* data, display_render_region_t region, uint8_t* buf, uint32_t i)
{
    display_shader_test_t* test = (display_shader_test_t*) data;

    /* Shader function stuff */
}

```
This is the working portion of your shader. For each line of the screen that is rendered, the corresponding shader function is called for each element of the render list in order. `data` is a pointer to your shader object, `region` contains the render region as passed into `display_render`, `buf` contains a buffer representing the state of the current line from `region.x1` to `region.x2`, and `i` identifies the current line being drawn to with reference to the top of the screen. Use the `display_shader_set_pixel`, `display_shader_get_pixel`, and `display_shader_set_line` functions to modify the buffer.

The shader function and the shader object are the main things needed to define a shader. The final part is to create a "new" function to set things up:
```
display_render_region_t display_shader_test_new(display_shader_test_t* test)
{
  display_shader_set_shader(test, &display_shader_test);

  /* Set things up */

  return (display_render_region_t) {.x1 = 0, .y1 = 0, .x2 = 127, .y2 = 127 };
}
```
This function is not strictly necessary but brings your new shader up to speed with other shaders provided by default with the driver. As already discussed, this function sets up your shader object for rendering then returns a `display_render_region_t` representing the minimal area covered by the object. The most important line here is the call to `display_shader_set_shader`, which links your previously created shader function to the new shader object. 

That is it! You can now use your new shader for fun and profit.

Projects
--------
As you have probably gathered, this driver is very powerful and has a lot of potential. The code here only scratches the surface of what it is capable of. Here are some ideas for things which might be fun to put some time into:

- The driver already includes a triangle shader, 3D is just around the corner!
- Shaders are capable of altering the existing image, blurs and other effects are definitely possible!
- The display driver has a "core" section which handles device commands and DMA at a low level, it may be fun to implement video streaming here!

If anybody implements something from this list, or something I have not thought of, feel free to send me a patch!
